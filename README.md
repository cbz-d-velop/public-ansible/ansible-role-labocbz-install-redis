# Ansible role: labocbz.install_redis

![Licence Status](https://img.shields.io/badge/licence-MIT-brightgreen)
![Testing Method](https://img.shields.io/badge/Testing%20Method-Ansible%20Molecule-blueviolet)
![Testing Driver](https://img.shields.io/badge/Testing%20Driver-docker-blueviolet)
![Language Status](https://img.shields.io/badge/language-Ansible-red)
![Compagny](https://img.shields.io/badge/Compagny-Labo--CBZ-blue)
![Author](https://img.shields.io/badge/Author-Lord%20Robin%20Crombez-blue)

## Description

![Tag: Ansible](https://img.shields.io/badge/Tech-Ansible-orange)
![Tag: Debian](https://img.shields.io/badge/Tech-Debian-orange)
![Tag: Ubuntu](https://img.shields.io/badge/Tech-Ubuntu-orange)
![Tag: Redis](https://img.shields.io/badge/Tech-Redis-orange)

An Ansible role to install and configure a Redis Standalone / Cluster server on your host.

The Ansible role installs Redis on a target system and offers additional features to facilitate the deployment of Redis as a service. The role supports the installation and execution of Redis in service mode. Additionally, if you want to set up a Redis cluster, it provides the flexibility to create multiple Redis services based on the chosen port number, for example, redis@6378. This enables running multiple Redis instances on a single machine, making it feasible to host three Redis servers on three individual machines, ultimately achieving the required nine Redis servers (3 masters + 2 replicas each).

In the context of a Redis cluster, to establish three master nodes, you would need a total of nine servers, taking into account both master and replica nodes. However, this configuration can be costly due to the number of required servers. To address this concern, the role proposes an alternative approach, allowing the creation of multiple Redis services on a single machine, each identified by a unique port number. By doing so, you can distribute the Redis services across fewer physical servers while still achieving the desired nine Redis servers.

The role's configurability is achieved through variables, enabling you to define the desired Redis configuration, including the bind address, port, protected mode, log level, and authentication settings. You can also set up SSL support with custom SSL certificate paths. Additionally, the role provides options for Redis cluster-related settings, such as specifying the number of replicas and whether to bootstrap the cluster.

In summary, the Redis role simplifies the installation and deployment of Redis, offering the flexibility to create Redis services with customizable settings, facilitating the setup of Redis clusters while optimizing resource utilization. Its rich configuration options empower administrators to tailor Redis deployments to their specific requirements, providing a robust solution for efficient and scalable Redis infrastructures.

## Usage

```SHELL
# Install husky and init
npm i && npx husky init && npm i validate-branch-name && npm cz

# Initialize the local secrets database, if not already present
MSYS_NO_PATHCONV=1 docker run -it --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest detect-secrets scan --exclude-files 'node_modules' > .secrets.baseline

# Decrypt the local Ansible Vault
# Get the Ansible Local Vault Key on PassBolt and create the .ansible-vault.key file, after that you can unlock your local vault
MSYS_NO_PATHCONV=1 docker run --rm -it -v "$HOME/.docker:/root/.docker" -v "$(pwd):/root/ansible/${DIR}" -w /root/ansible/${DIR} labocbz/ansible-molecule:latest /bin/sh -c 'cat ./.ansible-vault.key > /tmp/.ansible-vault.key && chmod -x /tmp/.ansible-vault.key && ansible-vault decrypt --vault-password-file /tmp/.ansible-vault.key ./tests/inventory/group_vars/vault.yml'
```

### Linters

```SHELL
# Analyse the project for secrets, and audit
./detect-secrets

# Lint Markdown
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest markdownlint './**.md' --disable MD013

# Lint YAML
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest yamllint -c ./.yamllint .

# Lint Ansible
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest ansible-lint --offline --exclude node_modules -x meta-no-info -p .
```

### Local Tests

This repository contain a unique Molecule controller to start containers and tests your roles and playbook inside them.

```SHELL
# Start the Molecule controller
./molecule-controller

# Inside the controller
molecule create
molecule converge
molecule verify
molecule test
```

### Use

In order to use this role, you have to know its defaults vars and their purppose.

### Vars

This role have these default vars here: [defaults.yml](./defaults/main.yml).

### Import and Run

To run this role or use it inside a playbook, import the call task from [converge.yml](./molecule/default/converge.yml).

## Architectural Decisions Records

Here you can put your change to keep a trace of your work and decisions.

### 2023-05-15: First Init

* First init of this role with the bootstrap_role playbook by Lord Robin Crombez
* Enabled SSL/TLS, but disable it after testing clustering ... (join cluster hangs up)
* A group "redis" can be use to set a group of ports, in order to install multiple Redis instance of the same host
* Clustering is possible by create (1 master + 2 replicas)x N, so if 3 servers are available for a Redis cluster, we have to install 3 Redis instance on it.

### 2023-05-30: Cryptographic update

* SSL/TLS Materials are not handled by the role
* Certs/CA have to be installed previously/after this role use

### 2023-10-06: New CICD, new Images

* New CI/CD scenario name
* Molecule now use remote Docker image by Lord Robin Crombez
* Molecule now use custom Docker image in CI/CD by env vars
* New CICD with needs and optimization

### 2023-12-14: System users

* Role can now use system users and address groups

### 2024-02-21: Fix and CI

* Added support for new CI base
* Edit all vars with __
* Added test
* Added multi cluster with Molecule
* Added cluster start (Sentinel)
* Removed idempotency (Sentinel edition files)

### 2024-05-19: New CI

* Added Markdown lint to the CICD
* Rework all Docker images
* Change CICD vars convention
* New workers
* Removed all automation based on branch

### 2024-10-26: Global refactoring

* Added one local Ansible Vault
* Edited gitignore file
* Add some commands in documentation
* Refactored the role

### 2024-12-31: New CICD and images

* Edited all Docker images
* Rework on the CICD
* Enabled SonarQube

### 2025-01-05: Certificates update

* Edit for use the latest version of import_certificates

## Authors

* Lord Robin Crombez

## Sources

* [Ansible role documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html)
* [Ansible Molecule documentation](https://molecule.readthedocs.io/)
